<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlessingLabel extends Model
{
    const TYPE_FAMILY = 'family';

    const TYPE_CAREER = 'career';

    const TYPE_LOVE = 'love';

    const TYPE_FORTUNE = 'fortune';

    use SoftDeletes;

    public static function typeKeys()
    {
        return array_keys(static::typeMap());
    }

    public static function typeMap()
    {
        return [
            self::TYPE_FAMILY => 'family',
            self::TYPE_CAREER => 'career',
            self::TYPE_LOVE => 'love',
            self::TYPE_FORTUNE => 'fortune',
        ];
    }
}
