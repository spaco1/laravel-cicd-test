<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBlessingLabel extends Model
{
    use SoftDeletes;

    public function blessingLabel()
    {
        return $this->belongsTo(BlessingLabel::class);
    }
}
