<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlessingLabelRequest;
use App\Http\Resources\BlessingLabelResource;
use App\Models\BlessingLabel;
use App\Models\UserBlessingLabel;
use App\Models\UserFamilyBook;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class FamilyBookController extends Controller
{
    const HOST = 'http://www.fougeng.com/frontend/yxh/activity-text2.html';

    public function index(Request $request)
    {
        $phone = $request->input('phone');

        /** @var UserFamilyBook $existedFamilyBook */
        $existedFamilyBook = UserFamilyBook::where('phone', $phone)->first();
        if (! $existedFamilyBook) {
            return $this->success(['data' => null]);
        }
        //QrCode::format('png')->size(250)->generate($this->generateQrCodeUrl($existedFamilyBook), public_path($this->generateQrCodeUrlSaveUrl($existedFamilyBook->id)));

        return $this->success([
            'data' => $this->qrCodeUrl($existedFamilyBook->id),
            'content' => $existedFamilyBook->content,
        ]);
    }

    private function generateQrCodeUrl(UserFamilyBook $userFamilyBook)
    {
        Log::info('generateQrCodeUrl', [self::HOST.'?phone='.$userFamilyBook->phone]);

        return self::HOST.'?phone='.$userFamilyBook->phone;
    }

    private function generateQrCodeUrlSaveUrl($id)
    {
        return $id.'sdgaseghsghytsfssf.png';
    }

    private function qrCodeUrl($id)
    {
        return 'http://www.fougeng.com/'.$id.'sdgaseghsghytsfssf.png';
    }

    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'phone' => 'required',
        ]);
        $phone = $request->input('phone');

        $existedFamilyBook = UserFamilyBook::where('phone', $phone)->first();
        if ($existedFamilyBook) {
            return $this->success(['data' => $this->qrCodeUrl($existedFamilyBook->id)]);
        }

        $familyBook = new UserFamilyBook();
        $familyBook->phone = $phone;
        $familyBook->content = $request->input('content');
        $familyBook->save();
        QrCode::format('png')->size(250)->generate($this->generateQrCodeUrl($familyBook), public_path($this->generateQrCodeUrlSaveUrl($familyBook->id)));

        return $this->success(['data' => $this->qrCodeUrl($familyBook->id)]);
    }
}
