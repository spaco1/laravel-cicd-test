<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlessingLabelRequest;
use App\Http\Resources\BlessingLabelResource;
use App\Models\BlessingLabel;
use App\Models\UserBlessingLabel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BlessingLabelController extends Controller
{
    public function store(BlessingLabelRequest $request)
    {
        $phone = $request->input('phone');
        $type = $request->input('type');
        $existedUserLabel = UserBlessingLabel::whereHas('blessingLabel')->where('phone', $phone)->first();
        if ($existedUserLabel) {
            return BlessingLabelResource::make($existedUserLabel->blessingLabel);
        }
        $label = BlessingLabel::whereType($type)->inRandomOrder()->firstOrFail();
        $userLabel = new UserBlessingLabel();
        $userLabel->phone = $phone;
        $userLabel->blessing_label_id = $label->id;
        $userLabel->save();

        return BlessingLabelResource::make($label);
    }

    public function index(Request $request)
    {
        $phone = $request->input('phone');
        $userLabel = UserBlessingLabel::wherePhone($phone)->first();
        if (! $userLabel) {
            return $this->success(['type' => 'none']);
        }
        $res = [
            'type' => $userLabel->blessingLabel->type,
            'time' => $userLabel->created_at->format('Y-m-d H:i'),
            'label' => $userLabel->blessingLabel->label,
        ];

        return $this->success($res);
    }

    public function show($id)
    {
        echo $id;
    }
}
