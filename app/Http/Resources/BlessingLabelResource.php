<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlessingLabelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var \App\Models\BlessingLabel $label */
        $label = $this->resource;

        return [
            'id' => $label->id,
            'label' => $label->label,
            'type' => $label->type,
        ];
    }
}
