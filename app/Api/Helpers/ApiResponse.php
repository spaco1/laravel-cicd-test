<?php

namespace App\Api\Helpers;

use Symfony\Component\HttpFoundation\Response as FoundationResponse;
use Response;

trait ApiResponse
{
    /**
     * @var int
     */
    protected $statusCode = FoundationResponse::HTTP_OK;

    /**
     * @var int
     */
    protected $httpCode = FoundationResponse::HTTP_OK;

    protected $data = null;

    protected $message = null;

    protected static $codeKey = 'code';

    protected static $dataKey = 'data';

    protected static $messageKey = 'message';

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function setHttpCode(int $httpCode)
    {
        $this->httpCode = $httpCode;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode(int $statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param array $header
     * @return mixed
     */
    private function respond($header = [])
    {
        $data = [
            self::$codeKey => $this->getStatusCode(),
            self::$dataKey => $this->getData(),
            self::$messageKey => $this->getMessage(),
        ];

        return Response::json($data, $this->getHttpCode(), $header);
    }

    public function failed(
        $httpCode = FoundationResponse::HTTP_BAD_REQUEST,
        $statusCode = FoundationResponse::HTTP_BAD_REQUEST,
        $message = null
    ) {
        $this->setStatusCode($statusCode);
        $this->setHttpCode($httpCode);
        $this->setMessage($message);

        return $this->respond();
    }

    /**
     * @return mixed
     */
    public function created($data = null)
    {
        $this->setHttpCode(FoundationResponse::HTTP_CREATED);

        return Response::json($data, $this->getHttpCode());
    }

    public function success($data = null)
    {

        return Response::json($data, $this->getHttpCode());
    }

}